<?php

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Blog\HomeController@index');
Route::get('/temp/post/{post}', 'Blog\HomeController@post')->name('pst');
Route::post('/mpesa','MpesaController@index')->name('mpesa');


Auth::routes(
    [ 'register'=>false
    ]
);

Route::get('users/{id}', function ($id) {
    return View('auth.login');
});


Route::group(['middleware' => 'auth'], function() {
Route::get('posts/restore/{post}','PostsController@restored')->name('posts.restore');
Route::get('posts/trashed','PostsController@trashed')->name('posts.trashed');
Route::get('/home', 'HomeController@index')->name('home');
Route::resource('category', 'CategoriesController');
Route::resource('posts','PostsController');
Route::resource('tag','TagsController');
Route::resource('subcategory','SubcategoryController');
Route::resource('user','UsersController');
Route::get('image','ImageController@index')->name('imagine');
Route::post('image/create','ImageController@create')->name('crop');
});

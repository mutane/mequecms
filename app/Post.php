<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Category;
use App\User;
use App\Tag;
class Post extends Model
{

     use SoftDeletes;
    protected $fillable = [
        'author_id','slider_id','title','content','description','published_at','image','video','youtube_link','category_id'
    ];
    


    public function category()
    {
        return $this->belongsTo(Category::class);
    }

     public function author()
    {
        return $this->belongsTo(User::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class)->withTimestamps();
    }

    public function hasTag($tag_id)
    {
        
    }
}

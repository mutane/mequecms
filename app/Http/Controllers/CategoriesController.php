<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Category\Create;
use App\Http\Requests\Category\Update;
class CategoriesController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.category')->with('categories',Category::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Create $request)
    {

         Category::create([
            'name'=>$request->name
        ]);    

         session()->flash('success','category created');
        return redirect(route('category.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, Category $category)
    {
        $category->name = $request->name;
        $category->save();
        session()->flash('success','category updated');
        return redirect(route('category.index'));        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        if ($category->posts !=null && $category->posts->count()> 0) {
            
            session()->flash('error','Não e\' possível apagar Categoria porque tem posts associados');
            return redirect()->back();
        }
        if ($category->subcategory !=null && $category->subcategory->count()> 0) {
            
            session()->flash('error','Não e\' possível apagar Categoria porque tem subcategorias associadas');
            return redirect()->back();
        }


        $category->delete();
        session()->flash('success','category deleted');
        return redirect(route('category.index'));
    }
}

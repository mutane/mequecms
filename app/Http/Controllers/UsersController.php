<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\User\Create;
use App\Http\Requests\User\Update;
use App\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.user')->with('users',User::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('auth.register')->with('users',User::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Create $request)
    {



          if ($request->image!=null || $request->image !="") {
             $dat = $request->image;
             $image_arr1 = explode(";", $dat);
             $image_arr2 = explode(",",$image_arr1[1]);
             $dat = base64_decode($image_arr2[1]);
             $imageName = Str::uuid()->toString().'.png';
             file_put_contents('storage/user/'.$imageName, $dat);
             $image = 'user/'.$imageName;
        }else{
                 $image ="user/avatar.jpg";
            }


            User::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'image'=>$image,
            'password'=>Hash::make($request->password)
        ]);
        
         session()->flash('success','User created Sucessful');
        return redirect(route('user.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
         return view('auth.register')->with('user',$user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, User $user)
    {
         
        $data = $request->only(
        [
            'name','email','password',
        ]);
  

        if ($request->image!=null || $request->image !="") {
             $dat = $request->image;
             $image_arr1 = explode(";", $dat);
             $image_arr2 = explode(",",$image_arr1[1]);
             $dat = base64_decode($image_arr2[1]);
             $imageName = Str::uuid()->toString().'.png';
             file_put_contents('storage/user/'.$imageName, $dat);
             $data['image'] = 'user/'.$imageName;
        }


         $data['password'] =  Hash::make($data['password']);
         $user->update($data);

          session()->flash('success','User updated sucessful');
        return redirect(route('user.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        session()->flash('success','User deleted sucessful');
        return redirect(route('user.index'));
    }
}

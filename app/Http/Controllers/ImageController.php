<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;



class ImageController extends Controller
{
   
    public function index()
    {
        return view('image.image');
    }

    public function create(Request $request)
    {


      $data = $request->image;
      $image_arr1 = explode(";", $data);
      $image_arr2 = explode(",",$image_arr1[1]);
      $data = base64_decode($image_arr2[1]);
      $imageName = Str::uuid()->toString().'.png';
        file_put_contents('storage/images/'.$imageName, $data);
        return $imageName;  
    }



    
}
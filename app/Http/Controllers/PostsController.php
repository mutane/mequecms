<?php

namespace App\Http\Controllers;

use App\Post;
use App\Category;
use App\Tag;
use Illuminate\Http\Request;
use App\Http\Requests\Post\Create;
use App\Http\Requests\Post\Update;
use App\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PostsController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('countCat')->only(['create','store']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.post')->with('categories',Category::all())->with('posts',Post::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('pages.createPost')->with('categories',Category::all())->with('posts',Post::all())->with('tags',Tag::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Create $request)
    {
       // dd($request->all());
        $image =null;
        $video = null;
        if ($request->image!=null || $request->image !="") {
            $dat = $request->image;
            $image_arr1 = explode(";", $dat);
            $image_arr2 = explode(",",$image_arr1[1]);
            $dat = base64_decode($image_arr2[1]);
            $imageName = Str::uuid()->toString().'.png';
            file_put_contents('storage/post/'.$imageName, $dat);
            $image = 'post/'.$imageName;
            $data['image'] = $image;
       }else{
                $image ="post/avatar.jpg";
                $data['image'] = $image;
           }

        if ($request->hasFile('video')) {
            $video =  $request->video->store('post');
            
            $data['video'] = $video;
        }
       // dd($video);
        $post = Post::create([
            'title'=>$request->title,
            'description'=>$request->description,
            'image'=>$image,
            'video'=>$video,
            'content'=>$request->content,
            'published_at'=>$request->published_at,
            'youtube_link' =>$request->youtube_link,
            'category_id'=>$request->category_id,
            'author_id'=>$request->author_id,
        ]);

        if ($request->tags) {
            
            $post->tags()->attach($request->tags);
        }
        
        session()->flash('success','Post created');
        return redirect(route('posts.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
         return view('pages.createPost')->with('categories',Category::all())->with('post',$post)->with('tags',Tag::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, Post $post)

    {
       // dd($request->all());
        $data = $request->only(
        [
            'name','title','description','content','published_at','category_id','youtube_link'
        ]);
        if ($request->image!=null || $request->image !="") {
            $dat = $request->image;
            $image_arr1 = explode(";", $dat);
            $image_arr2 = explode(",",$image_arr1[1]);
            $dat = base64_decode($image_arr2[1]);
            $imageName = Str::uuid()->toString().'.png';
            file_put_contents('storage/post/'.$imageName, $dat);
            $image = 'post/'.$imageName;
            $data['image'] = $image;
       }
       
        if ($request->hasFile('video')) {
            Storage::delete($post->video);
            $video =  $request->video->store('post');

            $data['video'] = $video;
        }
        $post->update($data);

        if ($request->tags) {
            
            $post->tags()->sync($request->tags);
        }

          session()->flash('success','Post updated sucessful');
        return redirect(route('posts.index')); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
            $post = Post::withTrashed()->get()->where('id',$id)->first();

        if ($post->trashed()) {
            Storage::delete($post->image);
             $post->forceDelete();
             

             session()->flash('success','Post Deleted sucessful');
            return redirect(route('posts.trashed'));

        }else{
            $post->delete();
        }
        

        

        session()->flash('success','Post Trashed sucessful');
        return redirect(route('posts.index'));
    }

  public function trashed()
    {
        return view('pages.trashedPost')->with('posts',Post::withTrashed()->get());
    }

    public function restored($id)
    {$post = Post::withTrashed()->get()->where('id',$id)->first();
        $post->restore();
      session()->flash('success','Post restored sucessful');
        return redirect(route('posts.trashed'));
    }
}

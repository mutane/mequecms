-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3308
-- Generation Time: Oct 24, 2020 at 08:32 PM
-- Server version: 8.0.18
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cmstemp`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(8, 'Hip-Hop', '2020-10-21 11:05:16', '2020-10-21 11:05:16'),
(9, 'Zook', '2020-10-21 11:33:46', '2020-10-21 11:33:46'),
(10, 'Kizomba', '2020-10-21 11:47:55', '2020-10-21 11:47:55');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
CREATE TABLE IF NOT EXISTS `images` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(31, '2014_10_12_000000_create_users_table', 1),
(32, '2014_10_12_100000_create_password_resets_table', 1),
(33, '2019_08_19_000000_create_failed_jobs_table', 1),
(34, '2020_03_21_112648_create_posts_table', 1),
(35, '2020_03_21_191935_create_categories_table', 1),
(36, '2020_03_22_172709_create_tags_table', 1),
(37, '2020_03_22_175953_create_subcategories_table', 1),
(38, '2020_03_23_115512_create_images_table', 2),
(39, '2020_03_23_174155_create_post_tag_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `published_at` timestamp NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube_link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slider_id` int(11) DEFAULT NULL,
  `author_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `content`, `description`, `category_id`, `published_at`, `image`, `video`, `youtube_link`, `slider_id`, `author_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(13, 'Dj Mutas Vakotas Viva La vida', 'O Dj Mais visto na Programacao', 'Ultimo Lancamento', 9, '2020-10-21 11:11:29', 'post/8EN0TDZPjTnDWrbhhN73vKe5O4PTEVPaL8f0DmpB.jpeg', 'post/4XVHoH2hwC4fDg6RdRwlP0GEHVF1xtfZDwKaVH07.mp4', 'http://localhost/phpmyadmin/sql.php?db=cmstemp&table=posts&pos=0', NULL, 14, '2020-10-21 11:12:40', '2020-10-24 05:20:52', NULL),
(16, 'Zaka T', 'Manga no Mbunda', 'Sembezo', 9, '2020-10-22 17:24:42', 'post/1DB3BS8yn1gEbdc3FIkvlcGnaFUHzJynG0wmTfCm.jpeg', NULL, NULL, NULL, 13, '2020-10-22 17:25:27', '2020-10-24 05:20:56', NULL),
(15, 'Pellentesque vitae pulvinar tortor nullam interdum metus ass', 'Pellentesque vitae pulvinar tortor nullam interdum metus a', 'Pellentesque vitae pulvinar tortor nullam interdum metus a', 10, '2020-10-21 11:50:42', 'post/qE2J3MAb0SzBMIIdIgpyyN14sQUGEfbaU9uXSNuD.jpeg', NULL, NULL, NULL, 14, '2020-10-21 11:51:00', '2020-10-24 05:20:57', NULL),
(14, 'Dj Lezio Me cola', 'A Melhor musica do momento', 'Pellentesque vitae pulvinar tortor nullam interdum metus a', 8, '2024-10-11 14:08:04', 'post/7cEdfY8cct5YS0cEMitmvamSZyICgsRXcG2WNt8d.jpeg', NULL, NULL, NULL, 14, '2020-10-21 11:50:15', '2020-10-24 05:21:00', NULL),
(17, 'Dj Lezio Me colas', 'ss', 'xcsd', 9, '2020-10-22 19:46:58', 'post/4cf4f870-e14f-4033-ae61-58c9b50476b2.png', NULL, 'http://localhost/phpmyadmin/sql.php?db=cmstemp&table=posts&pos=0s', NULL, 13, '2020-10-22 19:58:45', '2020-10-24 05:21:02', NULL),
(18, 'Zaka', 'd', 'ccc', 8, '2020-10-22 19:59:50', 'post/0ef2326b-a45d-4504-ae39-3a7ccb63e001.png', 'post/Zn4dB8xECEZTn4WWS2yJ89wSRhxBN0cydmWy2OkT.mp4', NULL, NULL, 13, '2020-10-22 20:00:23', '2020-10-22 21:10:38', NULL),
(19, 'music Tsss', 'dlklsma;\'\r\nA\'', 'c,xm cl dk', 8, '2020-10-22 20:01:15', 'post/23cd7d4d-eed8-4c25-aa86-44f4564693b0.png', NULL, NULL, NULL, 13, '2020-10-22 20:01:31', '2020-10-22 20:25:56', NULL),
(20, 'xxxxxx', 'a', 'x', 10, '2020-10-22 20:10:51', 'post/9a33a3d5-d042-4b00-b7b8-0bf32e45e395.png', 'post/srzz1PIBt7AHOvXz7Vtr1LsqRpUGYyn5RXZKgCm1.mp4', NULL, NULL, 13, '2020-10-22 20:11:19', '2020-10-22 20:57:36', NULL),
(21, 'Brantxos', 'hh', 'ss', 9, '2020-10-22 20:31:56', 'post/092913d3-fabc-4334-9f64-d9e503b0f304.png', NULL, NULL, NULL, 13, '2020-10-22 20:32:38', '2020-10-22 20:32:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `post_tag`
--

DROP TABLE IF EXISTS `post_tag`;
CREATE TABLE IF NOT EXISTS `post_tag` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_tag`
--

INSERT INTO `post_tag` (`id`, `post_id`, `tag_id`, `created_at`, `updated_at`) VALUES
(32, 18, 8, '2020-10-22 20:00:23', '2020-10-22 20:00:23'),
(31, 17, 8, '2020-10-22 19:58:45', '2020-10-22 19:58:45'),
(30, 16, 8, '2020-10-22 17:25:27', '2020-10-22 17:25:27'),
(28, 13, 8, '2020-10-21 11:12:41', '2020-10-21 11:12:41'),
(29, 14, 8, '2020-10-21 11:50:15', '2020-10-21 11:50:15'),
(33, 21, 8, '2020-10-24 05:13:37', '2020-10-24 05:13:37'),
(34, 21, 9, '2020-10-24 05:13:37', '2020-10-24 05:13:37'),
(35, 21, 10, '2020-10-24 05:13:37', '2020-10-24 05:13:37');

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

DROP TABLE IF EXISTS `subcategories`;
CREATE TABLE IF NOT EXISTS `subcategories` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
CREATE TABLE IF NOT EXISTS `tags` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `created_at`, `updated_at`) VALUES
(8, 'Covid-16', '2020-10-13 08:43:40', '2020-10-13 08:43:40'),
(9, 'tayob', '2020-10-24 05:12:54', '2020-10-24 05:12:54'),
(10, 'hop', '2020-10-24 05:13:07', '2020-10-24 05:13:07');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `image`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(16, 'Covid-18', 'admin@demao.com', 'user/e6b7fa7a-ff7b-4672-8899-e918453a878b.png', NULL, '$2y$10$yfwuNN4NCOhHDwbvk.4wCuXMPDCOFm34fIOx.LffQ7O1PdHO4tBkG', NULL, '2020-10-13 09:04:30', '2020-10-13 09:04:51'),
(15, 'created', 'admin@demo1.com', 'user/b9857a9c-156f-4814-b846-96310c368a45.png', NULL, '$2y$10$yfwuNN4NCOhHDwbvk.4wCuXMPDCOFm34fIOx.LffQ7O1PdHO4tBkG', NULL, '2020-10-13 08:56:31', '2020-10-13 08:56:31'),
(13, 'Admin', 'admin@admin.com', 'user/41efc78d-6d2f-4c00-9b34-4c8d31cd06fc.png', NULL, '$2y$10$yfwuNN4NCOhHDwbvk.4wCuXMPDCOFm34fIOx.LffQ7O1PdHO4tBkG', NULL, '2020-03-30 09:39:18', '2020-10-13 08:47:02'),
(14, 'Nelson', 'admin@demo.com', 'user/96ed3910-1c61-441c-b046-3604b15e5ac6.png', NULL, '$2y$10$yfwuNN4NCOhHDwbvk.4wCuXMPDCOFm34fIOx.LffQ7O1PdHO4tBkG', NULL, '2020-10-13 08:38:12', '2020-10-16 09:33:52');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

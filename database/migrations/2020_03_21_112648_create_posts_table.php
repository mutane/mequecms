<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id('id');
            $table->string('title');
            $table->text('content');
            $table->string('description');
            $table->integer('category_id');
            $table->timestamp('published_at');
            $table->string('image')->nullable();
            $table->string('video')->nullable();
            $table->string('youtube_link')->nullable();
            $table->integer('slider_id')->nullable();
            $table->integer('author_id');
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}

@extends('layouts.app')

@push('css')
   <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endpush
@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul class="list-group">
                @foreach ($errors->all() as $erro)
                    <li class="list-group-item text-danger">
                        {{$erro}}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif

 <div class="card shadow mb-4">
            <div class="card-header py-3 bg-gradient-primary ">
              <a href="#" class=" btn btn-sm btn-primary shadow-sm"   data-toggle="modal" data-target="#createSubcategory"><i class="fas fa-plus fa-sm text-white-50"></i>Create Subcategory</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Subcategory id</th>
                      <th>Subcategory name</th>
                      <th>Category of subcategory</th>
                      <th>Tags of subcategory</th>
                      <th>Posts of subcategory</th>
                      <th>Edit</th>
                      <th>Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                        @foreach ($subcategories as $subcategory)
                            <tr>
                                <td>{{$subcategory->id}}</td>
                                <td>{{$subcategory->name}}</td>
                                <td>{{$subcategory->category_id}}</td>
                                <td></td>
                                <td></td>
                                <td>
                                    <a href="#" class="btn btn-warning btn-circle btn-sm shadow-sm" onclick="update_cat({{$subcategory->id}},'{{$subcategory->name}}')"><i class="fas fa-edit"></i></a>
                                </td>
                                <td> 
                                    <a href="#" class="btn btn-danger btn-circle btn-sm shadow-sm" onclick="delete_cat({{$subcategory->id}})"><i class="fas fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach           
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          {{--Modal of create and update Subcategory--}}


  <div class="modal fade" id="createSubcategory" tabindex="-1" role="dialog" aria-labelledby="Subcategory" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-gradient-primary">
            <span class="font-weight-bold" id="Subcategory">New Subcategory</span>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            
            <form id="Subcategory_create" action="{{ route('subcategory.store') }}" method="POST">
                @csrf
                @method('POST')
                <div class="row">
                    <div class="col-sm-12">
                        <input type="text" name="name" class="form-control" placeholder="create Subcategory">
                    </div>
                    <div class=" py-2 col-sm-12">
                       <select name="category_id" class="form-control custom-select">
                            <option disabled="true" selected>Subcategory  category ...</option>
                            @foreach ($categories as $category)
                                <option  value="{{$category->id}}"
                                    @if (isset($subcategory) && ($category->id==$subcategory->category_id))
                                        selected 
                                    @endif
                                    >{{$category->name}}</option>
                            @endforeach
                       </select>
                    </div>
                </div>
            </form>

        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="{{ route('subcategory.store') }}" onclick="event.preventDefault();document.getElementById('Subcategory_create').submit();">Create</a>
          
        </div>
      </div>
    </div>
  </div>
{{-- update Subcategory modal--}}

  <div class="modal fade" id="UpdateSubcategory" tabindex="-1" role="dialog" aria-labelledby="Subcategory" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-gradient-warning">
          <span class="font-weight-bold" id="Subcategory_up">Update Subcategory</span>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            
            <form name="Update_Subcategory" id="Subcategory_update" method="POST">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-sm-12">
                        <input id="Subcategory_update_name" type="text" name="name" class="form-control" placeholder="update Subcategory">
                    </div>
                    <div class=" py-2 col-sm-12">
                       <select name="category_id" class="form-control custom-select">
                            <option disabled="true" selected>Subcategory  category ...</option>
                            @foreach ($categories as $category)
                                <option  value="{{$category->id}}"
                                    @if (isset($subcategory) && ($category->id==$subcategory->category_id))
                                        selected 
                                    @endif
                                    >{{$category->name}}</option>
                            @endforeach
                       </select>
                    </div>
                </div>
            </form>

        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary"  onclick="event.preventDefault();document.getElementById('Subcategory_update').submit();">Update</a>
          
        </div>
      </div>
    </div>
  </div>

{{--Modal of Delete Subcategory--}}




<div id="addNewAddonSubcategoryModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-gradient-danger">
                <h6 class="modal-title "><span class="font-weight-bold">Delete Subcategory</span></h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="alert alert-warning container">Are you sure ?</div>
                <form action="" method="POST" id="deleteCat">
                    @method('DELETE')
                    @csrf

               
                <button type="submit" class=" btn btn-primary btn-sm" data-dismiss="modal">
                        No . go back
                </button>
                <input type="submit" class="btn btn-danger btn-sm" value="yes . delete">
                        
                 </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function delete_cat(id){
        var form = document.getElementById('deleteCat');
        form.action = 'subcategory/'+id;
        console.log(form);
        $('#addNewAddonSubcategoryModal').modal('show');
    };

    function update_cat(id,name) {
        var form  = document.getElementById('Subcategory_update');
        form.action = 'subcategory/'+id;
        document.forms["Update_Subcategory"]["name"].value = name;
        console.log(form);
        $('#UpdateSubcategory').modal('show');
    };


  




</script>

@endsection

@push('js')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

  <!-- Page level custom scripts -->
  <script src="{{ asset('js/demo/datatables-demo.js') }}"></script>

@endpush
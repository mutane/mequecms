@extends('layouts.app')
@push('css')
   <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endpush

@section('content')


 <div class="card shadow mb-4">
            <div class="card-header py-3 bg-gradient-primary ">
              <a href=" {{ route('posts.create') }}" class=" btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i>Create Post</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Image</th>
                      <th>Post Title</th>
                      <th>Post Category</th>
                      <th>Posts  Author</th>
                      <th>Restore</th>
                      <th>Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                        @foreach ($posts as $post)
                              @if ($post->trashed())
                            <tr>
                                <td style="width: 120px; height: 30px"><img src="{{ asset('/storage') }}/{{$post->image}}" width="120px" height="30px"></td>
                                <td>{{$post->title}}</td>
                                <td>{{$post->category_id}}</td>
                                <td>{{$post->author_id}}</td>
                                
                                <td>
                                    <form action="{{ route('posts.restore',$post->id) }}" method="post">
                                        @csrf
                                        @method('get')
                                        <input type="submit" class="btn btn-primary btn-sm shadow-sm" value="Restore">
                                    </form>


                                    

                                </td>
                                <td> 
                                    <form action="{{ route('posts.destroy',$post->id) }}" method="POST">
                        @csrf
                        @method('DELETE')

                        <button type="submit" class="btn btn-danger btn-circle btn-sm shadow-sm"><i class="fas fa-trash"></i></button>
                      
                    </form>
                                    
                                </td>
                            </tr>
                            @endif
                        @endforeach           
                  </tbody>
                </table>
              </div>
            </div>
 </div>

@endsection

@push('js')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

  <!-- Page level custom scripts -->
  <script src="{{ asset('js/demo/datatables-demo.js') }}"></script>

@endpush
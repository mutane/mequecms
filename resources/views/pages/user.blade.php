@extends('layouts.app')

@push('css')
   <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endpush
@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul class="list-group">
                @foreach ($errors->all() as $erro)
                    <li class="list-group-item text-danger">
                        {{$erro}}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif


 <style>
     .user,.card-img-top{
        border-radius: 0rem !important;
        border:none !important;
     }
     .user{
       box-shadow:  0 14px 24px 0 rgba(50, 48, 57, 0.25);
     }
     .user-card{
        color: black;position: relative;
        font-style: bold;
        text-transform: capitalize;
        text-align: center;
        font-weight: bold;
     }
 </style>
<div class="container">
    <div>
    <a href=" {{ route('user.create') }}" class=" btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i>Create User</a></div>
    <div class="row">


    @foreach ($users as $user)
        <div class="col-md-6 col-lg-4 col-xl-3  col-sm-6 py-3">
        <div class="card m-b-30 user" style="border-radius: none">
                 <img class="card-img-top img-fluid" src="{{ asset('/storage') }}/{{$user->image}}" alt="Card image cap" style="height: 234.75px; " >
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item user-card">
                        <div class="row">
                                
                                <div class="col-sm-12">
                                  {{ __('User:') }}&nbsp<small>{{$user->name}}</small> &nbsp <span class="badge badge-success shadow-sm">Amin</span>
                                </div>
                                
                            </div>
                        </li>
                        <li class="list-group-item user-card">{{ __('Posts: ') }} &nbsp 43</li>
                      </ul>

                  <div class="card-body">
                    
                    <div class="row">
                                <div class="col-sm-6">
                                     <button onclick='location.href="{{ route('user.edit',$user->id) }}"' class="btn btn-warning  btn-sm "><i class="fas fa-edit"></i> &nbsp Edit</button>
                                </div>
                                
                                <div class="col-sm-6">
                                    <form  action="{{ route('user.destroy',$user->id) }}" method="POST">@csrf @method('DELETE')<button type="submit" class="btn btn-danger  btn-sm "><i class="fas fa-trash"></i>&nbsp Delete</button></form>
                                </div>
                            </div>
                            
                  </div>                                    
        </div>
        </div> 
    @endforeach
</div>    
</div>



 {{--<div class="card shadow mb-4">
            <div class="card-header py-3 bg-gradient-primary ">
              <a href=" {{ route('user.create') }}" class=" btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i>Create User</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#id</th>
                      <th>Image</th>
                      <th>Username</th>
                      <th>Email</th>
                      <th>Edit</th>
                      <th>Trash</th>
                    </tr>
                  </thead>
                  <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td>{{$user->id}}</td>
                                <td style="width: 120px; height: 30px"><img src="{{ asset('/storage') }}/{{$user->image}}" width="120px" height="30px"></td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                
                                <td>
                                    <a href="{{ route('user.edit',$user->id) }}" class="btn btn-warning btn-circle btn-sm shadow-sm"><i class="fas fa-edit"></i></a>
                                </td>
                                <td> 
                                    <form action="{{ route('user.destroy',$user->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger btn-circle btn-sm shadow-sm"><i class="fas fa-trash"></i></button>
                                      
                                    </form>
                                    
                                </td>
                            </tr>}}
                        @endforeach           
                  </tbody>
                </table>
              </div>
            </div>
 </div>--}}
@endsection

@push('js')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

  <!-- Page level custom scripts -->
  <script src="{{ asset('js/demo/datatables-demo.js') }}"></script>

@endpush
@extends('layouts.app')

@push('css')
<link href="{{ asset('assets/plugins/dropzone/dist/dropzone.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/plugins/dropify/css/dropify.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('crop/croppie.css') }}">
	{{-- expr --}}


        <link href="{{ asset('assets/plugins/timepicker/tempusdominus-bootstrap-4.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/plugins/clockpicker/jquery-clockpicker.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/plugins/colorpicker/asColorPicker.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />

        <link href="{{ asset('assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />



@endpush
@section('content')






          <div class="card shadow mb-4">

    <div class="card-header py-3 bg-gradient-primary">
        {{--isset($post ) ? $post:''--}}

        {{isset($post ) ? 'Edit Post':'Create Post'}}
    </div>
    <div class="card-body">

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul class="list-group">
                @foreach ($errors->all() as $erro)
                    <li class="list-group item text-danger">
                        {{$erro}}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif


        <form action="{{isset($post ) ? route('posts.update',$post->id):route('posts.store')}}" method="POST" enctype="multipart/form-data">
             @csrf
             @if (isset($post))
                 @method('PUT')
             @endif
            
            @guest
            @else
                <input type="hidden" name="author_id" value="{{Auth::user()->id}}">   
            @endguest
             
        
            <div class="row">
              
                <div class="form-group col-sm-6">
                    <input class="form-control" name="title" value="{{isset($post ) ? $post->title:''}}" id="title" placeholder="Post Title">
                </div>
                <div class="form-group col-sm-6">
                   <select name="category_id" class="form-control custom-select">
                        <option disabled="true" selected>Post  category ...</option>
                        @foreach ($categories as $category)
                            <option  value="{{$category->id}}"
                                @if (isset($post) && ($category->id==$post->category_id))
                                    selected 
                                @endif
                                >{{$category->name}}</option>
                        @endforeach
                   </select>
                </div>
            </div>
            <div class="row">

                <div class="form-group col-sm-6">
                    <textarea class="form-control" name="content"  id="content" placeholder="Post Content">{{isset($post ) ? $post->content:''}}</textarea>
                </div>
                 <div class="form-group col-sm-6">
                    <textarea class="form-control" name="description" id="description" placeholder="Post description">{{isset($post ) ? $post->description:''}}</textarea>
                </div>
            </div>
            <div class="row">
                <div class=" form-group col-sm-6 row">
                    <img  id="preview" 
                     @if (isset($post))
                        src="{{ asset('/storage') }}/{{$post->image}}" 
                     @else
                        src="{{asset('images/s20.jpg') }}" 
                     @endif
                    

                    class="col-sm-6 ">
                    <a class=" btn btn  bg-gradient-primary  col-sm-6 align-content-center" href="#" data-toggle="modal" data-target="#cropModal">
                  <i class="fas fa-image fa-sm fa-fw mr-2 text-gray-400"></i>
                  Post Image
                </a>


                 <div class="modal fade" id="cropModal" tabindex="-1" role="dialog" aria-labelledby="cropModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="cropModalLabel">Crop Image</h5>
                          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div class="modal-body">
                            <div class="row container " style="height: 500px;z-index: 9000001">
                                            <div class=" form-group col-sm-12">
                                                <div class=" col-sm-12 row">
                                                   <div class="col-sm-12 col-md-8">
                                                     
                                                        <input id="fileUpload" class="form-control" type="file" name="upload_image">
                                                    
                                                   </div>
                                                   <div class="col-sm-4">
                                                       <button class="btn btn-warning" id="submit_crop"><i class="fas fa-crop"></i> Cortar</button>
                                                   </div>
                                                </div>
                                                <div id="image_demo"  class="py-5">
                                                    
                                                </div>
                                                   
                                                    <input type="hidden" name="image"  id="image" value="">
                                            </div>
                                        </div>
                        </div>

                      </div>
                    </div>
                  </div>
              </div>
                
               
               <div class="form-group col-sm-6 ml-4 ml-sm-0 ml-md-4">
                <input type="file" id="input-file-now-custom-3" class="dropify form-control" 
                @if (isset($post))
                     data-default-file ="{{url('storage')}}/{{$post->video}}"
                 @endif  name="video" placeholder="Video" />
                </div>
            </div>

            <div class="row">
                <div class="form-group col-sm-6">
                    <input  class="form-control" type="text" name="youtube_link" value="{{ isset($post)?$post->youtube_link:''}}" placeholder="Youtube link">
                </div>

                @if ($tags->count()>0)
                    <div class="form-group col-sm-6">
                    <select name="tags[]" class=" custom-select select2 mb-3 select2-multiple form-control" style="width: 100%" multiple="multiple" data-placeholder="Post Tags">
                        <optgroup label="Tags">
                        @foreach ($tags  as $tag)        
                          <option value="{{$tag->id}}"
                                
                                @if (isset($post) && in_array($tag->id,collect( $post->tags->pluck('id'))->toArray() ) )
                                    selected="true"    
                                @endif
                                >{{$tag->name}}</option>
                         @endforeach
                       </optgroup>
                    </select>
                </div>
                <div class="form-group col-sm-6">
                @else
                    <div class="form-group col-sm-6">
                @endif
                    <input type="text" name="published_at" class="form-control" placeholder="Published at" id="min-date" value="{{isset($post ) ? $post->published_at: (new DateTime('now'))->format('Y-m-d H:i:s')}}"> 
                </div>
                 <div class="form-group col-sm-6 ">
                <input type="submit" class="btn  bg-gradient-success col-sm-12" value="{{isset($post ) ? 'Update post':'Submit post'}}">  
            </div>

            </div>
            

            
           
        </form>
    </div>
</div>

    {{-- carregador de ficheiro
    <div class="row">
    

                    <div class="col-xl-12">
                        <div class="card m-b-30">
                            <div class="card-body">
                                <input type="file" id="input-file-now-custom-3" class="dropify" data-height="500"/>
                            </div>
                        </div>
                    </div>       
    </div>--}}


      <!-- Logout Modal-->
 
@endsection

@push('js')

<script src="{{ asset('assets/plugins/dropzone/dist/dropzone.js') }}"></script>
<script src="{{ asset('assets/plugins/dropify/js/dropify.min.js') }}"></script>
	
      <script>
            $(document).ready(function(){
                // Basic
                $('.dropify').dropify({
                    messages: {
                        default: 'Post Video',
                        replace: 'Atualizar o ficheiro',
                        remove:  'Remover',
                        error:   'Erro, ficheiro não encontrado'
                    }
                });


                // Used events
                var drEvent = $('#input-file-events').dropify();

                drEvent.on('dropify.beforeClear', function(event, element){
                    return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
                });

                drEvent.on('dropify.afterClear', function(event, element){
                    alert('File deleted');
                });

                drEvent.on('dropify.errors', function(event, element){
                    console.log('Has Errors');
                });

                var drDestroy = $('#input-file-to-destroy').dropify();
                drDestroy = drDestroy.data('dropify')
                $('#toggleDropify').on('click', function(e){
                    e.preventDefault();
                    if (drDestroy.isDropified()) {
                        drDestroy.destroy();
                    } else {
                        drDestroy.init();
                    }
                })
            });
        </script> 



                 <!-- Plugins js -->
         <script src="{{ asset('assets/plugins/timepicker/moment.js') }}"></script>
         <script src="{{ asset('assets/plugins/timepicker/tempusdominus-bootstrap-4.js') }}"></script>
         <script src="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.js') }}"></script>
         <script src="{{ asset('assets/plugins/clockpicker/jquery-clockpicker.min.js') }}"></script>
         <script src="{{ asset('assets/plugins/colorpicker/jquery-asColor.js') }}" type="text/javascript"></script>
         <script src="{{ asset('assets/plugins/colorpicker/jquery-asGradient.js') }}" type="text/javascript"></script>
         <script src="{{ asset('assets/plugins/colorpicker/jquery-asColorPicker.min.js') }}" type="text/javascript"></script>
         <script src="{{ asset('assets/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
 
         <script src="{{ asset('assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
         <script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
         <script src="{{ asset('assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
         <script src="{{ asset('assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js') }}" type="text/javascript"></script>
 
         <!-- Plugins Init js -->
         <script src="{{ asset('assets/pages/form-advanced.js') }}"></script>
         <script src="{{ asset('crop/croppie.js') }}"></script>
    
         <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ready(function() {
            $image_crop = $('#image_demo').croppie({
                enableExif: true,
                viewport: {
                    width: 400,
                    height: 400,
                    type: 'square'
                },
                boundary: {
                    width:'auto',
                    height:'50vh'
                }
            });



              var isValidImage = false;

            $('#fileUpload').on('change', function() {
                
                 var fileInput = $(this);


                 
                  if (fileInput.length && fileInput[0].files && fileInput[0].files.length) {
                          var url = window.URL || window.webkitURL;
                          var image = new Image();
                          image.onload = function() {
                            
                           // alert('Valid Image');
                            isValidImage = true;
                          };
                          image.onerror = function() {
                            isValidImage = false;
                           // alert('Invalid image');
                          };
                          image.src = url.createObjectURL(fileInput[0].files[0]);
                        }
                var reader = new FileReader();
               reader.onload = function (event) {
                    $image_crop.croppie('bind',{
                     url: event.target.result 
                    }).then(function () {});}
                reader.readAsDataURL(this.files[0]);
             
            });


            $(document).on('click', '#submit_crop', function(event) {
                console.log(isValidImage);
               event.preventDefault()
                $image_crop.croppie('result',{
                    type:'canvas',
                    size:'viewport'
                }).then(function (response) {
                $('#preview')[0].attributes.src.value =  response; 
                console.log(response);
                console.log($('#preview')[0].attributes.src );
                    
                   if (isValidImage) {
                    $('#image').val(response);
                       $("#alert-session-js-js").remove();
                        $("#alert-session").remove();
                         $('#alert-session-js').append('<div id="alert-session-js-js" class="alert alert-session alert-dismissible fadeInDown animated  show container col-sm-8 --green" data-dimissble="alert">Image Cropped</div>');
                   }else{
                     $("#alert-session-js-js").remove();
                        $("#alert-session").remove();
                         $('#alert-session-js').append('<div id="alert-session-js-js" class="alert alert-danger alert-dismissible fadeInDown animated  show container col-sm-8 --green" data-dimissble="alert">Invalid Image</div>');
                        }
                            
        }); 

       });


});
    </script>
@endpush
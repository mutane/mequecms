<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sing Moz</title>

    <link rel="stylesheet" href="{{ asset('css/themify-icons.css') }}">
    <Favicon icon>
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon.png') }}">
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}"-->
    <link rel="stylesheet" href="{{ asset('css/video-player.css') }}"-->
    <link rel="stylesheet" href="{{ asset('icons/css/all.css') }}"-->
    <script src="{{ asset('icons/js/all.js') }}"></script>
    <script src="{{ asset('css/app.css') }}"></script>
    <style>
        .banner-wrapper .right-wrap {
            flex: 0 0 55%;
            max-width: 100%;
            position: relative;
            height: 100%;
            background-size: cover;
            margin: auto;
            margin-bottom: 30px;
            border-radius: 25px;

        }
        .banner-wrapper .left-wrap .btn-lg, .banner-wrapper .left-wrap .btn-group-lg > .btn{
            line-height: 0 !important;
        }
    </style>
    <!--script src="vfront/app.css"></script-->
</head>

<body>

    <div class="preloader"></div>

    <div class="main-wrapper">
        <!-- header wrapper -->
        <div class="header-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 navbar p-0">
                        <a href="{{ url('/') }}" class="logo"><img src="{{ asset('images/logo.png') }}" alt="logo" class="light"><img src="{{ asset('images/logo-white.png') }}" alt="logo" class="dark"></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            <ul class="navbar-nav nav-menu float-none text-center">
                                <li class="nav-item"><a class="nav-link" href="{{ asset('') }}">Pagina Inicial</a></li>
                                <li class="nav-item"><a class="nav-link" href="{{ asset('') }}">Categorias</a></li>
                                <li class="nav-item"><a class="nav-link" href="{{ asset('') }}">Sobre Nos</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">

                        <div class="user-avater">
                            <img src="{{ asset('images/u12.png') }}" alt="user">
                            <div class="user-menu">
                                <ul>
                                    @auth
                                    <li><a href="{{ url('/home') }}"><i class="ti-home"></i>Home</a></li>
                                    @else
                                    <li><a href="{{ route('login') }}"> <i class="ti-user"></i>Login</a></li>

                                    @if (Route::has('register'))
                                       <li><a href="{{ route('register') }}"><i class="ti-user"></i>Register</a></li>
                                    @endif
                                    @endauth
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- header wrapper -->
        <!-- banenr wrapper -->
        <!-- banenr wrapper -->
        <div class="banner-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="banner-wrap justify-content-between align-items-center">
                            <div class="left-wrap py-1 p-3 ml-3">
                                <span class="tag"><b>{{ $playlist->category->name}}</b></span>
                                <a href="video.html" class="btn btn-lg" data-toggle="modal" data-target="#ExemploModalCentralizado">
                                    <i class="fa fa-cart-arrow-down mr-2"></i>Comprar</a>
                            </div>
                            @php
                                echo '<div class="embed-responsive embed-responsive-21by9 mb-4" style="background:#000">';
                                echo    $playlist->youtube_link;
                                echo '</div>';


                            @endphp

                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="slide-wrapper">
                        <div class="slide-slider owl-carousel owl-theme">

                             @foreach ($playlist->category->post as $post)
                             <div class="owl-items">
                                <a class="slide-one slide-two slide-three" href="{{ route('pst',$post->id)}}">
                                    <div class="slide-image" style="background-image: url('{{ asset('storage/'.$post->image) }}');"></div>
                                    <div class="slide-content">
                                        <h2>{{ $playlist->category->name }}<img src="{{ asset('images/plus.png') }}" alt="icon"></h2>
                                        <span class="tag">{{ $post->title }}</span>
                                        <span class="tag"> <b><i class="fa fa-eye"></i></b> </span>
                                        <span class="tag"><b>{{ (new DateTime($post->created_at))->format('Y') }}</b></span>

                                    </div>
                                </a>
                            </div>
                             @endforeach

                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- banenr wrapper -->
        <!-- banenr wrapper -->
        <!-- slider wrapper -->

            @foreach($categories as $category)
            <div class="slide-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 text-left mb-4 mt-1">
                        <h2>{{$category->name}}</h2>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="slide-slider owl-carousel owl-theme">
                            @foreach($posts as $post)
                                @if($category->id == $post->category->id)
                                <div class="owl-items">
                                    <a class="slide-one" href="{{ route('pst',$post->id)}}">
                                        <div class="slide-image"><img src="{{ asset('/storage') }}/{{$post->image}}" alt="image"></div>
                                        <div class="slide-content">
                                            <h2>{{$post->title}} <img src="{{ asset('images/plus.png') }}" alt="icon" class="add-wishlist"></h2>
                                            <p>{{$post->content}}</p>
                                            <span class="tag"><i class="fa fa-user"></i> {{$post->author->name}}</span>
                                            <span class="tag">{{(new DateTime($post->published_at))->format('Y')}}</span>
                                            <span class="tag"><b>HD</b></span>
                                            <span class="tag"><b>16+</b></span>
                                        </div>
                                    </a>
                                </div>
                                @endif
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
            @endforeach

        <!-- slider wrapper -->
        <!-- footer wrapper -->
        <div class="footer-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 offset-sm-1 text-right">
                        <form action="#" class="mt-0">
                            <input type="text" placeholder="Pesquise o seu video">
                            <button>Pesquisar Video</button>
                        </form>
                    </div>
                    <div class="col-sm-12">
                        <div class="middle-footer">
                            <div class="row">
                                <div class="col-md-4 col-lg-2 col-sm-6 col-xs-6 md-mb25">
                                    <h5>Social Link</h5>
                                    <ul>
                                        <li><a href="#">Facebook</a></li>
                                        <li><a href="#">Twitter</a></li>
                                        <li><a href="#">Instagram</a></li>
                                        <li><a href="#">Youtube</a></li>
                                        <li><a href="#">Dribble</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-4 col-lg-2 col-sm-6 col-xs-6 md-mb25">
                                    <h5>ONLINE</h5>
                                    <ul>
                                        <li><a href="#">Web</a></li>
                                        <li><a href="#">Series</a></li>
                                        <li><a href="#">Natak</a></li>
                                        <li><a href="#">Comedy</a></li>
                                        <li><a href="#">Action</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-4 col-lg-2 col-sm-6 col-xs-6 md-mb25">
                                    <h5>Language</h5>
                                    <ul>
                                        <li><a href="#">English</a></li>
                                        <li><a href="#">Spanish</a></li>
                                        <li><a href="#">Arab</a></li>
                                        <li><a href="#">Urdu</a></li>
                                        <li><a href="#">Brazil</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-4 col-lg-2 col-sm-6 col-xs-6">
                                    <h5>Channel</h5>
                                    <ul>
                                        <li><a href="#">Makeup</a></li>
                                        <li><a href="#">Dresses</a></li>
                                        <li><a href="#">Girls</a></li>
                                        <li><a href="#">Sandals</a></li>
                                        <li><a href="#">Headphones</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-4 col-lg-2 col-sm-6 col-xs-6">
                                    <h5>About</h5>
                                    <ul>
                                        <li><a href="#">FAQ</a></li>
                                        <li><a href="#">Term of use</a></li>
                                        <li><a href="#">Privacy Policy</a></li>
                                        <li><a href="#">Feedback</a></li>
                                        <li><a href="#">Careers</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-4 col-lg-2 col-sm-6 col-xs-6">
                                    <h5 class="mb-3">Office</h5>
                                    <p style="width: 100%;">41 madison ave, floor 24 new work, NY 10010 <br>1-877-932-7111</p>
                                    <p style="width: 100%;">41 madison ave, floor 24 new work, NY 10010 <br>1-877-932-7111</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 lower-footer"></div>
                    <div class="col-sm-6">
                        <p class="copyright-text">© 2020 copyright. All rights reserved.</p>
                    </div>
                    <div class="col-sm-6 text-right">
                        <p class="float-right copyright-text">In case of any concern, <a href="#">contact us</a></p>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer wrapper -->

    </div>

    <div class="player-container lightbox" style="display: none">
        <a href="#" class="close-video-player btn-lightbox-close"><i class="ti-close"></i></a>
        <div class='player'>
            <video id='video' src='images/video1.mp4' playsinline></video>
                <div class='play-btn-big'></div>
                <div class='controls'>
                    <div class="time"><span class="time-current"></span><span class="time-total"></span></div>
                    <div class='progress'>
                        <div class='progress-filled'></div>
                    </div>
                    <div class='controls-main'>
                        <div class='controls-left'>
                            <div class='volume'>
                                <div class='volume-btn loud'>
                                    <svg width="26" height="24" viewBox="0 0 26 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M6.75497 17.6928H2C0.89543 17.6928 0 16.7973 0 15.6928V8.30611C0 7.20152 0.895431 6.30611 2 6.30611H6.75504L13.9555 0.237289C14.6058 -0.310807 15.6 0.151473 15.6 1.00191V22.997C15.6 23.8475 14.6058 24.3098 13.9555 23.7617L6.75497 17.6928Z" transform="translate(0 0.000518799)" fill="white"/>
                                        <path id="volume-low" d="M0 9.87787C2.87188 9.87787 5.2 7.66663 5.2 4.93893C5.2 2.21124 2.87188 0 0 0V2C1.86563 2 3.2 3.41162 3.2 4.93893C3.2 6.46625 1.86563 7.87787 0 7.87787V9.87787Z" transform="translate(17.3333 7.44955)" fill="white"/>

                                        <path id="volume-high" d="M0 16.4631C4.78647 16.4631 8.66667 12.7777 8.66667 8.23157C8.66667 3.68539 4.78647 0 0 0V2C3.78022 2 6.66667 4.88577 6.66667 8.23157C6.66667 11.5773 3.78022 14.4631 0 14.4631V16.4631Z" transform="translate(17.3333 4.15689)" fill="white"/>
                                <path id="volume-off" d="M1.22565 0L0 1.16412L3.06413 4.0744L0 6.98471L1.22565 8.14883L4.28978 5.23853L7.35391 8.14883L8.57956 6.98471L5.51544 4.0744L8.57956 1.16412L7.35391 0L4.28978 2.91031L1.22565 0Z" transform="translate(17.3769 8.31403)" fill="white"/>
                                    </svg>

                                </div>
                                <div class='volume-slider'>
                                    <div class='volume-filled'></div>
                                </div>
                            </div>
                        </div>
                        <div class='play-btn paused'></div>
                        <div class="controls-right">
                            <div class='speed'>
                                <ul class='speed-list'>
                                    <li class='speed-item' data-speed='0.5'>0.5x</li>
                                    <li class='speed-item' data-speed='0.75'>0.75x</li>
                                    <li class='speed-item active' data-speed='1'>1x</li>
                                    <li class='speed-item' data-speed='1.5'>1.5x</li>
                                    <li class='speed-item' data-speed='2'>2x</li>
                                </ul>
                            </div>
                            <div class='fullscreen'>
                                <svg width="30" height="22" viewBox="0 0 30 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0 0V-1.5H-1.5V0H0ZM0 18H-1.5V19.5H0V18ZM26 18V19.5H27.5V18H26ZM26 0H27.5V-1.5H26V0ZM1.5 6.54545V0H-1.5V6.54545H1.5ZM0 1.5H10.1111V-1.5H0V1.5ZM-1.5 11.4545V18H1.5V11.4545H-1.5ZM0 19.5H10.1111V16.5H0V19.5ZM24.5 11.4545V18H27.5V11.4545H24.5ZM26 16.5H15.8889V19.5H26V16.5ZM27.5 6.54545V0H24.5V6.54545H27.5ZM26 -1.5H15.8889V1.5H26V-1.5Z" transform="translate(2 2)" fill="white"/>
                                </svg>
                            </div>
                        </div>


                </div>
            </div>
        </div>
    </div>

  <!-- Modal -->
  <div class="modal fade" id="ExemploModalCentralizado" tabindex="-1" role="dialog" aria-labelledby="TituloModalCentralizado" aria-hidden="true">
    <div class="modal-dialog modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="TituloModalCentralizado">Efectuar Pagamento</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('mpesa') }}" method="post">
            <div class="modal-body">

                @csrf
                <div class="form-group">
                <label>Numero do Mpesa</label>
                <input type="number" class="form-control col-sm-8" placeholder="Inserir o numero" name="contact">
                </div>
            </div>
            <div class="modal-footer">

            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-primary">Processar Pagamento</button>
            </div>
        </form>
      </div>
    </div>
  </div>




    <script src="{{ asset('js/plugin.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/scripts.js') }}"></script>
    <script src="{{ asset('js/video-player.js') }}"></script>

</body>
</html>

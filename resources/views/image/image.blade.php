@extends('layouts.app')

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('crop/croppie.css') }}">
@endpush

@section('content')
   <div class="container">
       <div class="card">
           <div class="card-header">
               <h4 style="color: black">demo upload</h4>
           </div>
           <div class="card-body">
               <div class="" id="image_demo" >
                   
               </div>


               <div >
                   <img src="" id="imagexf" hidden>
               </div>
                <form  id="crop" method="post" action="{{ route('crop') }}">
                    @csrf
                    <input type="file" name="upload_image" id="upload_image">
                    <input type="hidden" name="image"  id="image" value="">
                    <button id="submit_crop">submit</button>
                </form>
              
           </div>
       </div>
   </div>
@endsection

@push('js')
    <script type="text/javascript" src="{{ asset('crop/croppie.min.js') }}"></script>
    <script type="text/javascript">

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ready(function() {
            $image_crop = $('#image_demo').croppie({
                enableExif: true,
                viewport: {
                    width: 200,
                    height: 200,
                    type: 'square'
                },
                boundary: {
                    width:'auto',
                    height:'50vh'
                }
            });


var isValidImage = false;

            $('#upload_image').on('change', function() {
                
                 var fileInput = $(this);


                 
                  if (fileInput.length && fileInput[0].files && fileInput[0].files.length) {
                          var url = window.URL || window.webkitURL;
                          var image = new Image();
                          image.onload = function() {
                            
                            alert('Valid Image');
                            isValidImage = true;
                          };
                          image.onerror = function() {
                            isValidImage = false;
                            alert('Invalid image');
                          };
                          image.src = url.createObjectURL(fileInput[0].files[0]);
                        }
                var reader = new FileReader();
               reader.onload = function (event) {
                    $image_crop.croppie('bind',{
                        url: event.target.result
                    }).then(function () {});}
                reader.readAsDataURL(this.files[0]);
             
            });


            $(document).on('click', '#submit_crop', function(event) {
                console.log(isValidImage);
               event.preventDefault()
                $image_crop.croppie('result',{
                    type:'canvas',
                    size:'viewport'
                }).then(function (response) {
                    console.log(response);
                    $("#image").val(response);
                   var form =  $('#crop').serializeArray();
                   
                   if (isValidImage) {
                        $.ajax({
                           url: '{{ route('crop') }}',
                           type: 'POST',
                           data: form,
                           success:function (response) {
                            $('#imagexf')[0].src=response;
                            $('#imagexf')[0].hidden = false;
                            $("#alert-session").remove();
                             $("#alert-session-js-js").remove();
                            $('#alert-session-js').append('<div id="alert-session" class="alert alert-session alert-dismissible fadeInDown animated  show container col-sm-8 --green">Update image</div>');
                              
                           }
                       });
                   }else{
                     $("#alert-session-js-js").remove();
                        $("#alert-session").remove();
                         $('#alert-session-js').append('<div id="alert-session-js-js" class="alert alert-danger alert-dismissible fadeInDown animated  show container col-sm-8 --green" data-dimissble="alert">Invalid Image</div>');

                   }
                   
            });
        }); 
        });
        /*
                   $.ajax({
                       url: '{{ route('crop') }}',
                       type: 'POST',
                       data: form,
                       success:function (response) {
                        $('#imagexf')[0].src=response;
                        $('#imagexf')[0].hidden = false;
                          
                       }
                   });
                   */
    </script>
@endpush
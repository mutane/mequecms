@extends('layouts.app')



@push('css')
<link href="{{ asset('assets/plugins/dropzone/dist/dropzone.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/plugins/dropify/css/dropify.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('crop/croppie.css') }}">
<style type="text/css">
    /*
*
* ==========================================
* CUSTOM UTIL CLASSES
* ==========================================
*
*/

.file-upload input[type='file'] {
  display: none;
}

/*
*
* ==========================================
* FOR DEMO PURPOSES
* ==========================================
*
*/

body {
  background: #00B4DB;
  background: -webkit-linear-gradient(to right, #0083B0, #00B4DB);
  background: linear-gradient(to right, #0083B0, #00B4DB);
  height: 100vh;
}

.rounded-lg {
  border-radius: 1rem;
}

.custom-file-label.rounded-pill {
  border-radius: 50rem;
}

.custom-file-label.rounded-pill::after {
  border-radius: 0 50rem 50rem 0;
}
</style>


@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-gradient-primary"> {{isset($user ) ? __('Update  User') :'Create New User'}}</div>

                <div class="card-body">
                    <form method="POST" action="{{isset($user ) ? route('user.update',$user->id) : route('user.store')}}" enctype="multipart/form-data" id="crop">
                        @csrf

                        @if (isset($user ))
                             @method('PUT')
                        @endif
                      

                        <div class="form-group row">
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" required autocomplete="name" autofocus placeholder="Insert Username"  value="{{isset($user ) ? $user->name:old('name')}}">

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror


                            </div>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" required autocomplete="email" placeholder="Insert email" value="{{isset($user ) ? $user->email:old('email')}}">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                       <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Insert password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm password">
                            </div>
                        </div>

                       
                         <div class="row container">
                            <div class=" form-group col-sm-12">
                                <div class=" col-sm-12 row">
                                   <div class="col-sm-12 col-md-8">
                                      <label for="fileUpload" class="file-upload btn btn-primary btn-block"><i class="fa fa-upload mr-2" ></i>upload
                                        <input id="fileUpload" type="file" name="upload_image">
                                    </label> 
                                   </div>
                                   <div class="col-sm-4">
                                       <button class="btn btn-warning" id="submit_crop"><i class="fas fa-crop"></i> Cortar</button>
                                   </div>
                                </div>
                                <div id="image_demo" >
                                    
                                </div>
                                   
                                    <input type="hidden" name="image"  id="image" value="">
                            </div>
                        </div>

                        <div class="form-group row mb-0 container">
                            <div class="col-md-12 ">
                                <button type="submit" class="col-sm-12 btn btn-primary">
                                    {{isset($user ) ? __('Update'):__('Register')}}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')

<script src="{{ asset('assets/plugins/dropzone/dist/dropzone.js') }}"></script>
<script src="{{ asset('assets/plugins/dropify/js/dropify.min.js') }}"></script>
<script src="{{ asset('crop/croppie.js') }}"></script>
    
      <script>
            $(document).ready(function(){
                // Basic
                $('.dropify').dropify({
                    messages: {
                        default: 'Post Image or Video',
                        replace: 'Atualizar o ficheiro',
                        remove:  'Remover',
                        error:   'Erro, ficheiro não encontrado'
                    }
                });


                // Used events
                var drEvent = $('#input-file-events').dropify();

                drEvent.on('dropify.beforeClear', function(event, element){
                    return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
                });

                drEvent.on('dropify.afterClear', function(event, element){
                    alert('File deleted');
                });

                drEvent.on('dropify.errors', function(event, element){
                    console.log('Has Errors');
                });

                var drDestroy = $('#input-file-to-destroy').dropify();
                drDestroy = drDestroy.data('dropify')
                $('#toggleDropify').on('click', function(e){
                    e.preventDefault();
                    if (drDestroy.isDropified()) {
                        drDestroy.destroy();
                    } else {
                        drDestroy.init();
                    }
                })
            });
        </script> 


{{-- cropiie javascrip functions--}}

<script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ready(function() {
            $image_crop = $('#image_demo').croppie({
                enableExif: true,
                viewport: {
                    width: 200,
                    height: 200,
                    type: 'square'
                },
                boundary: {
                    width:'auto',
                    height:'50vh'
                }
            });



              var isValidImage = false;

            $('#fileUpload').on('change', function() {
                
                 var fileInput = $(this);


                 
                  if (fileInput.length && fileInput[0].files && fileInput[0].files.length) {
                          var url = window.URL || window.webkitURL;
                          var image = new Image();
                          image.onload = function() {
                            
                           // alert('Valid Image');
                            isValidImage = true;
                          };
                          image.onerror = function() {
                            isValidImage = false;
                           // alert('Invalid image');
                          };
                          image.src = url.createObjectURL(fileInput[0].files[0]);
                        }
                var reader = new FileReader();
               reader.onload = function (event) {
                    $image_crop.croppie('bind',{
                     url: event.target.result 
                    }).then(function () {});}
                reader.readAsDataURL(this.files[0]);
             
            });


            $(document).on('click', '#submit_crop', function(event) {
                console.log(isValidImage);
               event.preventDefault()
                $image_crop.croppie('result',{
                    type:'canvas',
                    size:'viewport'
                }).then(function (response) {
                    console.log(response);
                    
                   if (isValidImage) {
                    $('#image').val(response);
                       $("#alert-session-js-js").remove();
                        $("#alert-session").remove();
                         $('#alert-session-js').append('<div id="alert-session-js-js" class="alert alert-session alert-dismissible fadeInDown animated  show container col-sm-8 --green" data-dimissble="alert">Image Cropped</div>');
                   }else{
                     $("#alert-session-js-js").remove();
                        $("#alert-session").remove();
                         $('#alert-session-js').append('<div id="alert-session-js-js" class="alert alert-danger alert-dismissible fadeInDown animated  show container col-sm-8 --green" data-dimissble="alert">Invalid Image</div>');
                        }
                            
        }); 

       });


});
    </script>







@endpush



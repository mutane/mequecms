<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 <meta name="csrf-token" content="{{ csrf_token() }}">


  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Custom fonts for this template-->
  <link href="{{asset('vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">

  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">


  @stack('css')
</head>

<body id="page-top" class="sidebar-toggled">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion shadow-sm" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('/') }}">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Tayob<sup></sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item @if (Route::is('home')) {{ __('active') }} @endif">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

        <hr class="sidebar-divider my-0">
            
      <!-- Nav Item - Dashboard -->
      <li class="nav-item @if (Route::is('posts.index')) {{ __('active') }} @endif @if (Route::is('posts.create')) {{ __('active') }} @endif @if (Route::is('posts.edit')) {{ __('active') }} @endif">
        <a class="nav-link" href="{{ route('posts.index') }}" data-toggle="collapse" data-target="#cat_item" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-newspaper "></i>
          <span>Posts</span></a>

           <div id="cat_item" class="collapse @if (Route::is('posts.index')) {{ __('show') }} @endif @if (Route::is('posts.create')) {{ __('show') }} @endif @if (Route::is('posts.edit')) {{ __('show') }} @endif" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                  <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Post Crud:</h6>
                    <a class="collapse-item @if (Route::is('posts.index')) {{ __('active') }} @endif" href="{{ route('posts.index') }}" >List</a>
                    <a class="collapse-item @if (Route::is('posts.create')) {{ __('active') }} @endif @if (Route::is('posts.edit')) {{ __('active') }} @endif" href="{{ route('posts.create') }}">Create or Update</a>

                  </div>
                </div>
      </li>

      <!-- Nav Item - Dashboard -->
      <li class="nav-item @if (Route::is('category.index')) {{ __('active') }} @endif">
        <a class="nav-link" href="{{ route('category.index') }}" >
          <i class="fas fa-layer-group"></i>
          <span>Categories</span></a>
      </li>




      <!-- Nav Item - Dashboard -->
      <li class="nav-item @if (Route::is('tag.index')) {{ __('active') }} @endif">
        <a class="nav-link" href="{{ route('tag.index') }}">
          <i class="fas fa-tags fa-tachometer-alt"></i>
          <span>Tags</span></a>
      </li>


      <!-- Nav Item - Dashboard -->
      <li class="nav-item @if (Route::is('posts.trashed')) {{ __('active') }} @endif">
        <a class="nav-link" href="{{ route('posts.trashed') }}">
          <i class="fas fa-trash-restore "></i>
          <span>Trashed Posts</span></a>
      </li>


    

      <!-- Nav Item - Dashboard -->
      <li class="nav-item @if (Route::is('subcategory.index')) {{ __('active') }} @endif">
        <a class="nav-link" href="{{ route('subcategory.index') }}">
          <i class="fas fa-layer-group "></i>
          <span>Subcategories</span></a>
      </li>



      <!-- Nav Item - Dashboard -->
      <li class="nav-item @if (Route::is('user.index')) {{ __('active') }} @endif
        @if (Route::is('user.edit')) {{ __('active') }} @endif
        @if (Route::is('user.create')) {{ __('active') }} @endif
        ">
        <a class="nav-link" href="{{ route('user.index') }}" data-toggle="collapse" data-target="#user_item" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-users"></i>
          <span>Users</span></a>
           <div id="user_item" class="collapse @if (Route::is('user.index')) {{ __('show') }} @endif @if (Route::is('user.create')) {{ __('show') }} @endif @if (Route::is('user.edit')) {{ __('show') }} @endif" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                  <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">User Crud:</h6>
                    <a class="collapse-item @if (Route::is('user.index')) {{ __('active') }} @endif" href="{{ route('user.index') }}" >List</a>
                    <a class="collapse-item @if (Route::is('user.create')) {{ __('active') }} @endif @if (Route::is('user.edit')) {{ __('active') }} @endif" href="{{ route('user.create') }}">Create or Update</a>

                  </div>
                </div>

      </li>

    

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 sticky-top shadow-sm">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
          <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group">
              <input type="text" class="form-control small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto ">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>


            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->

           @guest
            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                            @else

            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{Auth::user()->name}}</span>
                <img class="img-profile rounded-circle" src="{{ asset('/storage') }}/{{Auth::user()->image}}" style="width: 24px; height: 24px">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="{{ route('user.edit',Auth::user()->id)}}" >
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>
            @endguest

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
                <main class="py-4">
                    <div id="alert-session-js">
                        
                    </div>
                      @if (session()->has('success'))
                            <div id="alert-session" class="alert alert-session alert-dismissible fadeInDown animated  show container col-sm-8 --green">
                                {{session()->get('success')}}
                            </div>
                        @endif
                        @if (session()->has('error'))
                            <div id="alert-danger" class="alert alert-danger alert-dismissible fadeInDown animated  show container col-sm-8 --green">
                                {{session()->get('error')}}
                            </div>
                        @endif
                    @yield('content')
                 </main>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>All rights reserved &copy; {{config('app.name','Laravel')}} {{date("Y")}}</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
        </div>
      </div>
    </div>
  </div>




  <!-- Bootstrap core JavaScript-->
  <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>


  @stack('js')
  <script type="text/javascript">
      $(document).ready(function() {
    // show the alert
    setTimeout(function() {
        $("#alert-session").addClass('fadeOutUp');
    }, 2000);
    setTimeout(function() {
        $("#alert-session").alert('close');
    }, 3000);

    setTimeout(function() {
        $("#alert-session-js").addClass('fadeOutUp');
    }, 2000);
    setTimeout(function() {
        $("#alert-session-js").alert('close');
    }, 3000);
    
});
  </script>

  <script type="text/javascript">
      $(document).ready(function() {
    // show the alert
    setTimeout(function() {
        $("#alert-danger").addClass('fadeOutUp');
    }, 2000);
    setTimeout(function() {
        $("#alert-danger").alert('close');
    }, 3000);

    $(document).on('mousemove',function(){
         setTimeout(function() {
        $("#alert-session-js-js").addClass('fadeOutUp');
    }, 2000);
    setTimeout(function() {
        $("#alert-session-js-js").alert('close');
    }, 3000);
    
    });

    
});
  </script>
</body>

</html>
